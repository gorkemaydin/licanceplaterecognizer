//
//  ViewController.swift
//  LicencePlateRecognizer
//
//  Created by Görkem Aydın on 13.08.2019.
//  Copyright © 2019 GorkemAydin. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {
    
    var frameExtractor: FrameExtractor!
    
    @IBOutlet private weak var uiPlate: UILabel!
    @IBOutlet private weak var uiImageView: UIImageView!
    
    private var isCityCodeFound = false
    private var isLettersFound = false
    private var isUUIDfound = false
    
    private var cityCode : String?
    private var letters : String?
    private var uuid : String?
    private var captureGoesOn = true
   
    @IBAction func tryAgainButton(_ sender: Any) {
        self.captureGoesOn = true
    }
    @IBAction private func findButton(_ sender: Any) {
        guard let plate = self.uiPlate.text?.trimmingCharacters(in: .whitespacesAndNewlines) else { return }
        HTTPLayer.getPlateDetail(by: plate) { Result in
            guard let result = Result else { return }
            self.uiPlate.text = result.Aciklama
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        frameExtractor = FrameExtractor()
        frameExtractor.delegate = self
    }
}
extension ViewController : FrameExtractorDelegate {
    func captured(image: UIImage) {
        uiImageView.image = image
        guard captureGoesOn else { return }
        startVision(image)
    }
    private func startVision(_ image : UIImage) {
        let vision = Vision.vision()
        let textRecognizer = vision.onDeviceTextRecognizer()
        let visionImage = VisionImage(image: image)
        textRecognizer.process(visionImage) { result, error in
            guard error == nil, let result = result else { return }
            self.validatePlate(result.text)
        }
    }
    private func validatePlate(_ text : String) {
        var plate = text.trimmingCharacters(in: .whitespacesAndNewlines)
        
        guard plate.count < 10 && plate.count > 4 && captureGoesOn else { return }
        
        
        
        if !isCityCodeFound {
            if let cityCode =  Int(text.prefix(2)), cityCode <= 81, cityCode > 0 {
                self.cityCode = String(text.prefix(2))
                self.isCityCodeFound = true
            }
        }
        plate = String(plate.dropFirst(2))
        
        if !isLettersFound {
            let letters = plate.components(separatedBy: CharacterSet.decimalDigits).joined().trimmingCharacters(in: .whitespacesAndNewlines)
            guard !letters.isEmpty , letters.count < 4 else { return }
            self.letters = letters
            self.isLettersFound = true
        }
        
        if !isUUIDfound {
            self.uuid = ""
            plate.forEach { (char) in
                if "0"..."9" ~= char {
                    self.uuid?.append(char)
                    self.isUUIDfound = true
                }
            }
        }
        
        if isUUIDfound && isLettersFound && isCityCodeFound {
            captureGoesOn = false
            var p = self.cityCode ?? "" + " "
            p += self.letters ?? "" + " "
            p += self.uuid ?? "" + " "
            self.uiPlate.text =  p
        }
    }
}
extension Int {
    static func parse(from string: String) -> Int? {
        return Int(string.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())
    }
}
