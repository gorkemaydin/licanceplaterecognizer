//
//  PlateDebitModal.swift
//  LicencePlateRecognizer
//
//  Created by Görkem Aydın on 13.08.2019.
//  Copyright © 2019 GorkemAydin. All rights reserved.
//

import Foundation

class PlateDebitModal : Codable {
    
    public var Aciklama : String?
    public var IndirimliBorc : Double = 0
    public var IslemKodu = 0
}
