//
//  ObjectMapperUtil.swift
//  LicencePlateRecognizer
//
//  Created by Görkem Aydın on 13.08.2019.
//  Copyright © 2019 GorkemAydin. All rights reserved.
//

import Foundation

class ObjectMapperUtil {
    
    static func MapJson<T : Codable>(_ data : Data , ofType : T.Type) -> T?  {
        return try? JSONDecoder().decode(T.self, from: data)
    }
}
