//
//  FrameExtractorDelegate.swift
//  LicencePlateRecognizer
//
//  Created by Görkem Aydın on 13.08.2019.
//  Copyright © 2019 GorkemAydin. All rights reserved.
//

import UIKit

protocol FrameExtractorDelegate: class {
    func captured(image: UIImage)
}
