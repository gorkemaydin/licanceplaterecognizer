//
//  HTTPRequest.swift
//  LicencePlateRecognizer
//
//  Created by Görkem Aydın on 13.08.2019.
//  Copyright © 2019 GorkemAydin. All rights reserved.
//

import Foundation
import Alamofire

class HTTPRequest {
    
    private let afManager = Alamofire.SessionManager()
    private let asyncGroup = DispatchGroup()
    
    public func Service<T: Codable> (method:String,
                                     path:String,
                                     additionalPath : String? = nil,
                                     parameters:[String:String]? = nil,
                                     requestObj: T?,
                                     complated : @escaping (_ some : Data?) -> Void = { _ in}){
        
        
        var _url = URLComponents(string: StaticConstants.BaseURL + path)
        
        if let AddPath = additionalPath {
            _url = URLComponents(string: StaticConstants.BaseURL + path  + AddPath)
        }
        
        var request = URLRequest(url: (_url?.url)!)
        
        request.httpMethod = method
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("tr-TR", forHTTPHeaderField: "Accept-Language")
        request.setValue("Basic aWJiOjEyMzQ=", forHTTPHeaderField: "Authorization")
        
        let pre = Locale.preferredLanguages[0]
        request.setValue(pre, forHTTPHeaderField: "lang")
        
        if(requestObj != nil){
            do {
                let jsonEncoder = JSONEncoder()
                request.httpBody = try jsonEncoder.encode(requestObj)
            } catch let jsonErr {
                print("failed to decode, \(jsonErr)")
            }
        }
        let task = self.beginBackgroundTask()
        request.timeoutInterval = 120
        
        asyncGroup.notify(queue: .main) {
            
            self.asyncGroup.enter()
            self.afManager.request(request).responseJSON {
                response in
                
                if let code = response.response?.statusCode {
                    if(code == 200){
                        complated(response.data)
                    }
                    self.asyncGroup.leave()
                    self.endBackgroundTask(taskID: task)
                }
            }
        }
    }
    private func beginBackgroundTask() -> UIBackgroundTaskIdentifier {
        return UIApplication.shared.beginBackgroundTask(expirationHandler: { })
    }
    
    private func endBackgroundTask(taskID: UIBackgroundTaskIdentifier) {
        UIApplication.shared.endBackgroundTask(taskID)
    }
}
