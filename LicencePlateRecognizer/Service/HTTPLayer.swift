//
//  HTTPLayer.swift
//  LicencePlateRecognizer
//
//  Created by Görkem Aydın on 13.08.2019.
//  Copyright © 2019 GorkemAydin. All rights reserved.
//

import Foundation

class HTTPLayer {
    
    static func getPlateDetail(by plate : String,complated : @escaping (PlateDebitModal?) -> ()) {
       
        HTTPRequest().Service(method: "GET",
                              path: ServicePaths.plateDebit,
                              additionalPath: nil,
                              parameters: nil,
                              requestObj: AnyCodable(nil))
        { (JData) in
            guard let data = JData else { return }
            
            complated(ObjectMapperUtil.MapJson(data, ofType: PlateDebitModal.self))
        }
    }
}


