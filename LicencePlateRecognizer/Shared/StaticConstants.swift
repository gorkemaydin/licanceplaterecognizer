//
//  StaticConstants.swift
//  LicencePlateRecognizer
//
//  Created by Görkem Aydın on 13.08.2019.
//  Copyright © 2019 GorkemAydin. All rights reserved.
//

import Foundation

class StaticConstants {
    static let BaseURL = "https://mobilservis.ibb.gov.tr"
    static let NullCodable = AnyCodable([:])
}
